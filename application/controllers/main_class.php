<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class main_class extends CI_Controller {

	public function index(){
		$this->viewLoader('main_page');
	}
	
	public function viewLoader($view, $header = 'headers/default', $footer = 'footers/default', $params = array('header' => array(), 'view' => array(), 'footer' => array())){
		if(!$view){
			return -1;
		}
		$this->load->view($header, $params['header']);
		$this->load->view($view, $params['view']);
		$this->load->view($footer, $params['footer']);
	}
}
